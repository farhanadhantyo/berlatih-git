<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Cast;

class CastController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth')->except('index', 'show');
    }
    
    public function create()
    {
        return view('cast.create');
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'nama' => 'required',
    		'umur' => 'required',
            'bio' => 'required'
    	]);
 
        Cast::create([
    		'nama' => $request->nama,
    		'umur' => $request->umur,
            'bio' => $request->bio
    	]);
 
    	return redirect('/cast');
    }

    public function index()
    {
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    public function show($id)
    {
        $cast = Cast::find($id);
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $cast = cast::find($id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast->delete();
        return redirect('/cast');
    }
}
