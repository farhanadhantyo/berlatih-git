<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page.Register');
    }
    public function kirim(Request $request)
    {
        $namaDepan = $request->input('fname');
        $namaBelakang = $request->input('lname');

        return view('page.Welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    } 
}
