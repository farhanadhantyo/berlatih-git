<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\KritikController;








/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'utama'])->name('home');
Route::get('/register', [AuthController::class, 'daftar'])->name('daftar');
Route::post('/welcome', [AuthController::class, 'kirim'])->name('submit');
Route::get('/data-table', function(){
    return view('page.table');
});

Route::middleware(['auth'])->group(function () {
    ////CRUD Cast

    //Create
    //Form Tambah Cast
    Route::get('/cast/create', [CastController::class, 'create'])->name('buat');
    //Untuk Kirim Data Ke Database Atau Tambah Data Ke Database
    Route::post('/cast', [CastController::class, 'store'])->name('indeks2');

    
    //Update
    //Form Update Kategori
    Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit'])->name('mengedit');
    //Update Data Ke Database Berdasarkan Id
    Route::put('/cast/{cast_id}', [CastController::class, 'update'])->name('pembaruan');

    //Delete
    //Delete Berdasarkan Id
    Route::delete('/cast/{cast_id}', [CastController::class, 'destroy'])->name('hancur');
    
    //Profile
    Route::post('/cast', [CastController::class, 'store'])->name('indeks2');
});

//Read
    //Tampil Semua
    Route::get('/cast', [CastController::class, 'index'])->name('indeks');
    //Detail Cast Berdasarkan Id
    Route::get('/cast/{cast_id}', [CastController::class, 'show'])->name('tampil');





Auth::routes();


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('user', [UserController::class, 'index']);
Route::get('genre', [GenreController::class, 'index']);


