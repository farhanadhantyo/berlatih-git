@extends('layout.master')
@section('judul')
Register
@endsection

@section('content')
<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>

<form action="/welcome" method="POST">
    @csrf
    <label>First Name:</label>
    <br>
    <br>
    <input type="text" name="fname">
    <br>
    <br>
    <label>Last Name:</label>
    <br>
    <br>
    <input type="text" name="lname">
    <br>
    <br>
    <label>Gender</label>
    <br>
    <br>
    <input type="radio">Male
    <br>
    <input type="radio">Female
    <br>
    <br>
    <label>Nationality</label>
    <br>
    <br>
    <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="singapura">Singapura</option>
        <option value="thailand">Thailand</option>
    </select>
    <br>
    <br>
    Language Spoken
    <br>
    <br>
    <input type="checkbox" name="language">Bahasa Indonesia
    <br>
    <input type="checkbox" name="language">English
    <br>
    <input type="checkbox" name="language">Other
    <br>
    <br>
    <label>Bio</label>
    <br>
    <br>
    <textarea name="bio" cols="30" rows="10"></textarea>
    <br>
    <input type="submit" value="Sign Up">
</form>
@endsection
    