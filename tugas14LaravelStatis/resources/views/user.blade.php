@extends('layout.master')
@section('judul')
User
@endsection

@section('content')
<h3>One to one</h3>
<table>
  <thead>
   <tr>
    <th>name</th>
    <th>email</th>
    <th>umur</th>
   </tr>
  <tbody>
   @foreach($user as $value)
   <tr>
    <td>{{$value->name}}</td>
    <td>{{$value->email}}</td>
    <td>{{$value->profile->umur}}</td>
   </tr>
   @endforeach
  <tbody>
  </thead>
</table>
@endsection